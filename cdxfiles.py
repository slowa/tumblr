#!/usr/bin/env python

# Download all the CDX files.

import internetarchive as ia

size_bytes = 0

for result in ia.search_items('collection:archiveteam_tumblr'):
    item = ia.get_item(result['identifier'])
    for file in item.files:
        if file['name'].endswith('cdx.gz'):
            print(file['name'], file['size'])
            size_bytes += int(file['size'])

size_gb = size_bytes / 1024 / 1024 / 1024
print(size_gb)



