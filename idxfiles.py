#!/usr/bin/env python3

# We're not entirely sure what an idx file is, but it seems to be a CDX like
# file that includes a SURT version of a URL that has been archived. Since these
# are much smaller than the CDX files we can fetch all of them, parse out the URLs
# and write them to a file data/urls.txt

import internetarchive as ia

def get_url(cdx_line):
    """
    This functions takes a line IDX line and returns the URL it contains after
    converting it from the SURT format.
    """
    parts = cdx_line.split('\t')
    if len(parts) > 0 and parts[0]:
        surt, ts = parts[0].split()
        surt_host, path = surt.split(')', 1)
        host_parts = surt_host.split(',')
        host_parts.reverse()
        host = '.'.join(host_parts)
        return f'https://{host}{path}'
    else:
        return None

fh = open('data/urls.txt', 'w')
for result in ia.search_items('collection:archiveteam_tumblr'):
    item = ia.get_item(result['identifier'])
    for file in item.files:
        if file['name'].endswith('idx'):
            resp = item.download(return_responses=True, files=[file['name']], silent=True)[0]
            for line in resp.text.splitlines():
                url = get_url(line)
                if url:
                    fh.write(url + '\n')
