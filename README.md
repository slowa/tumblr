# tumblr

A few things for looking at the [Tumblr Internet Archive collection]:

    pip3 install -r requirements
    python3 collect.py archiveteam_tumblr

<img src="https://gitlab.com/slowa/tumblr/-/raw/master/images/archiveteam_tumblr.png">

[Tumblr Internet Archive collection]: https://archive.org/details/archiveteam_tumblr

