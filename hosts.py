#!/usr/bin/env python

import csv

from collections import Counter
from urllib.parse import urlparse

# count the hosts
hosts = Counter()
for url in open('data/urls.txt'):
    uri = urlparse(url.strip())
    hosts[uri.netloc] += 1

# write out the host counts as a csv
out = csv.writer(open('data/hosts.csv', 'w'))
out.writerow(['host', 'count'])
for host, count in hosts.most_common():
    out.writerow([host, count])


